;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Mes.
;;;
;;; Mes is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Mes is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Mes.  If not, see <http://www.gnu.org/licenses/>.

(define-module (mes)
  #:use-module (8sync)
  #:use-module (mes gui))

(define-syntax define-preferences
  (syntax-rules ()
   ((_ (pref val))
    (define pref val))
   ((_ (pref val) (pref* val*) ...)
    (begin
      (define-preferences (pref val))
      (define-preferences (pref* val*) ...)))))

;;; Should be an executable bundled with Mes.  Can be redefined in the
;;; init file.
(define-once tahoe-executable "tahoe")
(define-once gateways '())

(define mes-config-dir (string-append
                        (or (getenv "XDG_CONFIG_HOME")
                            (let ((h (getenv "HOME")))
                              (when (not h)
                                (error "can't find $HOME")) ;internationalize
                              (string-append h
                                             (if (string-suffix? "/" h) "" "/")
                                             ".config")))
                        "/mes"))

(define init-file (string-append mes-config-dir "/init.scm"))

(define (file-exists? filename)
  (let ((st (stat init-file #f)))
    (and st (eq? (stat:type st) 'regular))))

(define (load-init-file)
  (when (file-exists? init-file)
    ;; TODO Load into a sandbox module
    (load init-file)))

(define-actor <mes> (<actor>)
  ((*init* mes-init)))

(define (mes-init actor message)
  (if (null? gateways)
      (display-welcome)
      (display "Start gateways!\n")))

(define (display-welcome)
  (display "Welcome!\n"))

(define-public (run-mes)
  (load-init-file)
  (let ((hive (make-hive)))
    (bootstrap-actor hive <mes>)
    (bootstrap-actor hive <gui>)
    (run-hive hive '())))

(if (resolve-module '(geiser) #f #:ensure #f)
    (use-modules (ice-9 threads))
    (run-mes))
