;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Mes.
;;;
;;; Mes is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Mes is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Mes.  If not, see <http://www.gnu.org/licenses/>.

(define-module (mes render)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-9)
  #:use-module (rnrs bytevectors)
  #:use-module (system foreign)
  #:use-module (gl)
  #:use-module (mes gl))

(define (compile-shader type shader)
  "Compile SHADER of TYPE"
  (let ((sh (gl-create-shader type)))
    (gl-shader-source sh shader)
    (gl-compile-shader sh)
    (unless (gl-get-shader sh compile-status)
      (let ((log (gl-get-shader-info-log sh)))
       (gl-delete-shader sh)
       (error "Shader compilation failed: ~%~A" log)))
    sh))

(define (free-shader-program program)
  (map (lambda (sh)
         (with-check-gl-error (gl-delete-shader sh)))
       (with-check-gl-error (gl-get-attached-shaders program)))
  (with-check-gl-error (gl-delete-program program)))

;;; emacs: (put 'with-cleanup 'scheme-indent-function 0)
(define-syntax-rule (with-cleanup thunk cleanup)
  (let ((cleanup-proc (lambda () cleanup)))
    (dynamic-wind
      (const #t)
      (lambda ()
        (let ((value thunk))
          (set! cleanup-proc (const #t))
          value))
      (lambda ()
        (cleanup-proc)))))

(define (compile-and-attach program type shader)
  (let ((sh (compile-shader type shader)))
    (with-cleanup
      (with-check-gl-error (gl-attach-shader program sh))
      (gl-delete-shader sh))))

(define (make-shader-program vertex fragment)
  (let ((pr (gl-create-program)))
    (if (= pr 0)
        (error "Failed to create a program"))
    (with-cleanup
      (begin
        (compile-and-attach pr 'vertex vertex)
        (compile-and-attach pr 'fragment fragment)
        (gl-link-program pr)
        (unless (gl-get-program pr link-status)
          (error "Program linking failed: ~%~A"
                 (gl-get-program-info-log pr))))
      (free-shader-program pr))
    pr))

(define (load-shaders vertex-shader fragment-shader . attrib-locations)
  (let ((program (make-shader-program vertex-shader fragment-shader)))
    (apply values (cons program
                        (with-cleanup
                          (map (lambda (attr)
                                 (with-check-gl-error
                                  (gl-get-attrib-location program attr)))
                               attrib-locations)
                          (free-shader-program program))))))

(define-record-type <renderer>
  (make-renderer shader-program vertex-array vertex-buffer texture
                 attrib-position attrib-texposition attrib-color
                 uniform-view)
  renderer?
  (shader-program renderer-shader-program set-renderer-shader-program!)
  (vertex-array renderer-vertex-array set-renderer-vertex-array!)
  (vertex-buffer renderer-vertex-buffer set-renderer-vertex-buffer!)
  (texture renderer-texture set-renderer-texture!)
  (attrib-position renderer-attrib-position)
  (attrib-texposition renderer-attrib-texposition)
  (attrib-color renderer-attrib-color)
  (uniform-view renderer-uniform-view))

(define-public (close-render renderer)
  (match-let ((($ <renderer>
                  (and shader-program (set! set-shader-program!))
                  (and vertex-buffer (set! set-vertex-buffer!))
                  (and vertex-array (set! set-vertex-array!))
                  (and texture (set! set-texture!)))
               renderer))
    (when shader-program
      (free-shader-program shader-program)
      (set-shader-program! #f))
    (unless (zero? vertex-buffer)
      (gl-delete-buffer vertex-buffer)
      (set-vertex-buffer! 0))
    (unless (zero? vertex-array)
      (gl-delete-vertex-arrays (list vertex-array))
      (set-vertex-array! 0))
    (unless (zero? texture)
      (gl-delete-textures (list texture))
      (set-texture! 0))))

(define-public (init-render)
  (receive (program position texposition color)
      (load-shaders
       "attribute vec2 position;
        attribute vec2 texposition;
        attribute vec4 color;
        uniform mat4 view;
        varying vec2 frag_texposition;
        varying vec4 frag_color;
        void main()
        {
          gl_Position = view * vec4(position, 0., 1.);
          frag_texposition = texposition;
          frag_color = color;
        }"
       "varying vec2 frag_texposition;
        varying vec4 frag_color;
        uniform sampler2D sampler;
        void main()
        {
          gl_FragColor =
            (frag_color.x < 0.f) ?
            texture2D(sampler, frag_texposition) :
            frag_color;
        }"
       "position" "texposition" "color")
    (let ((r (make-renderer program 0 0 0 position texposition color
                            (gl-get-uniform-location program "view"))))
      (with-cleanup
        (begin
          (set-renderer-vertex-array! r (car (gl-gen-vertex-arrays 1)))
          (when (zero? (renderer-vertex-array r))
            (error "failed to generate a vertex array"))
          (set-renderer-vertex-buffer! r (gl-generate-buffer))
          (when (zero? (renderer-vertex-buffer r))
            (error "failed to generate a vertex buffer"))
          (set-renderer-texture! r (gl-generate-texture))
          (when (zero? (renderer-texture r))
            (error "failed to generate a texture")))
        (close-render r))
      r)))

(define* (make-rect x1 y1 x2 y2 #:key (color '(0 0 0 0)) texture)
  (let ((c (if texture
               (cons -1 (cdr color))    ;Don't use color, use texture
               color)))
    (list x1 y1 x2 y2 c texture)))
(export make-rect)

(define-public (draw-rects renderer frame-size-x frame-size-y rects)
  (let* ((len (length rects))
         ;; 6 vertices,
         ;; position = 2 floats,
         ;; texposition = 2 floats,
         ;; color = 4 floats.
         (vertex-size (* (sizeof float) (+ 2 2 4)))
         (bv (make-bytevector (* vertex-size 6 len)))
         (cp2 (lambda (i n1 n2)
                (f32vector-set! bv i n1)
                (f32vector-set! bv (1+ i) n2)))
         (cp4 (lambda (i n1 n2 n3 n4)
                (f32vector-set! bv i n1)
                (f32vector-set! bv (1+ i) n2)
                (f32vector-set! bv (+ 2 i) n3)
                (f32vector-set! bv (+ 3 i) n4)))
         (target (arb-vertex-buffer-object array-buffer-arb)))
    (let lp ((i 0)
             (rects rects))
      (when (pair? rects)
        (match-let (((x1 y1 x2 y2 (r g b a) texture) (car rects)))
          ;; TODO Load rect's texture
          (cp2 i x1 y1)                 ;Position
          (cp2 (+ 2 i) 0 0)             ;Texposition
          (cp4 (+ 4 i) r g b a)         ;Color

          (cp2 (+ 8 i) x2 y1)
          (cp2 (+ 10 i) 0 0)
          (cp4 (+ 12 i) r g b a)

          (cp2 (+ 16 i) x1 y2)
          (cp2 (+ 18 i) 0 0)
          (cp4 (+ 20 i) r g b a)

          (cp2 (+ 24 i) x1 y2)
          (cp2 (+ 26 i) 0 0)
          (cp4 (+ 28 i) r g b a)

          (cp2 (+ 32 i) x2 y2)
          (cp2 (+ 34 i) 0 0)
          (cp4 (+ 36 i) r g b a)

          (cp2 (+ 40 i) x2 y1)
          (cp2 (+ 42 i) 0 0)
          (cp4 (+ 44 i) r g b a))
        (lp (+ 48 i) (cdr rects))))
    (gl-enable (enable-cap blend))
    (gl-blend-equation (ext-blend-minmax func-add))
    (gl-blend-func (blending-factor-src src-alpha)
                   (blending-factor-src one-minus-src-alpha))
    (gl-use-program (renderer-shader-program renderer))
    (gl-uniform-matrix-4fv (renderer-uniform-view renderer)
                           1 (boolean false)
                           (f32vector (/ 2f0 frame-size-x) 0f0 0f0 -1f0
                                      0f0 (/ -2f0 frame-size-y) 0f0 1f0
                                      0f0 0f0 -1f0 0f0
                                      0f0 0f0  0f0 1f0))
    (gl-bind-vertex-array (renderer-vertex-array renderer))
    (gl-bind-buffer target (renderer-vertex-buffer renderer))
    (gl-buffer-data target (bytevector-length bv)
                    (bytevector->pointer bv)
                    (arb-vertex-buffer-object stream-draw-arb))
    (gl-vertex-attrib-pointer (renderer-attrib-position renderer)
                              2 'float #f vertex-size %null-pointer)
    (gl-vertex-attrib-pointer (renderer-attrib-texposition renderer)
                              2 'float #f vertex-size
                              (make-pointer (* (sizeof float) 2)))
    (gl-vertex-attrib-pointer (renderer-attrib-color renderer)
                              4 'float #f vertex-size
                              (make-pointer (* (sizeof float) 4)))
    (gl-enable-vertex-attrib-array (renderer-attrib-position renderer))
    (gl-enable-vertex-attrib-array (renderer-attrib-texposition renderer))
    (gl-enable-vertex-attrib-array (renderer-attrib-color renderer))
    (gl-draw-arrays (begin-mode triangles) 0 (* len 6))))
