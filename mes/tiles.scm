;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Mes.
;;;
;;; Mes is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Mes is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Mes.  If not, see <http://www.gnu.org/licenses/>.

(define-module (mes tiles)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-43)
  #:use-module (gl)
  #:use-module (8sync)
  #:use-module (mes render))

;;; Irregular grid.  The columns and the rows of the grid are not of
;;; equal size.
(define-record-type <grid>
  (make-grid array columns rows)
  grid?
  (array grid-array)
  ;; Element k of COLUMNS vector specifies the size of column k in
  ;; ARRAY.  The same for elements of ROWS vector.
  (columns grid-columns)
  (rows grid-rows))

(define (make-new-grid initial-value column-size row-size)
  (make-grid (make-array initial-value 1 1)
             (vector column-size)
             (vector row-size)))

(define-inlinable (grid-columns-length grid)
  (vector-length (grid-columns grid)))

(define-inlinable (grid-rows-length grid)
  (vector-length (grid-rows grid)))

(define-inlinable (grid-array-ref grid row column)
  (array-ref (grid-array grid) row column))

(define-inlinable (grid-array-set! grid obj row column)
  (array-set! (grid-array grid) obj row column))

(define-inlinable (vector-sum vector)
  (vector-fold (lambda (i s v) (+ s v)) 0 vector))

(define (location->index vector location)
  "Find an index for LOCATION.
Return two values: the location index and sum of all sizes in VECTOR
up to the index."
  (when (negative? location)
    (error "Location is negative: ~a" location))
  (let loop ((i 0)
             (sum (vector-ref vector 0)))
    (if (< location sum)
        (values i sum)
        (let ((i1 (1+ i)))
          (if (< i1 (vector-length vector))
              (loop i1 (+ sum (vector-ref vector i1)))
              (error "Location is out of grid bounds: ~a" location))))))

(define (insert-location vector location)
  (receive (index sum) (location->index vector location)
    (let* ((new (make-vector (1+ (vector-length vector))))
           (sum-loc (- sum location))
           (index-size (- (vector-ref vector index) sum-loc)))
      ;; Do not make zero-size cells
      (if (zero? index-size)
          (values vector index)
          (begin
            (vector-copy! new 0 vector 0 index)
            (vector-set! new index index-size)
            (vector-copy! new (1+ index) vector index)
            (vector-set! new (1+ index) sum-loc)
            (values new index))))))

(define (merge-index vector index)
  (let ((new (make-vector (1- (vector-length vector)))))
    (vector-copy! new 0 vector 0 index)
    (vector-set! new index (+ (vector-ref vector index)
                              (vector-ref vector (1+ index))))
    (when (< (1+ index) (vector-length new))
      (vector-copy! new (1+ index) vector (+ 2 index)))
    new))

(define (copy-rows! target tstart source sstart send)
  (do ((sr sstart (1+ sr))
       (tr tstart (1+ tr)))
      ((>= sr send))
    (array-cell-set! target
                     (vector-copy (array-cell-ref source sr))
                     tr)))

(define (split-row grid location proc)
  "Split a row of GRID at LOCATION.
Call (PROC cell) for each splitted cell.  PROC should return two
values for up and down cells."
  (receive (new-rows row)
      (insert-location (grid-rows grid) location)
    (if (eq? new-rows (grid-rows grid))
        (values grid row)
        (let* ((columns (vector-length (grid-columns grid)))
               (old-array (grid-array grid))
               (new-array (make-array #f
                                      (vector-length new-rows)
                                      columns)))
          (copy-rows! new-array 0 old-array 0 row)
          (copy-rows! new-array (+ 2 row)
                      old-array (1+ row) (vector-length (grid-rows grid)))
          (do ((i 0 (1+ i))
               (rowvec (array-cell-ref old-array row)))
              ((>= i columns))
            (receive (up down) (proc (array-ref rowvec i))
              (array-fill! (array-slice new-array row i)
                           up)
              (array-fill! (array-slice new-array (1+ row) i)
                           down)))
          (values (make-grid new-array (grid-columns grid) new-rows)
                  (+ 2 row))))))

(define (split-column grid location proc)
  "Split a column of GRID at LOCATION.
Call (PROC cell) for each splitted cell.  PROC should return two
values for left and right cells."
  (let ((old (make-grid (transpose-array (grid-array grid) 1 0)
                        (grid-rows grid)
                        (grid-columns grid))))
    (receive (new column) (split-row old location proc)
      (values (make-grid (transpose-array (grid-array new) 1 0)
                         (grid-rows new)
                         (grid-columns new))
              column))))

(define (merge-rows grid row proc)
  "Merge ROW with (1+ ROW).
For each two merging cells call (PROC up-cell down-cell) witch should
return a value for the new cell."
  (when (< (vector-length (grid-rows grid)) 2)
    (error "At least two grid rows are required."))
  (let* ((columns (vector-length (grid-columns grid)))
         (old-rows (grid-rows grid))
         (new-rows (merge-index old-rows row))
         (old-array (grid-array grid))
         (new-array (make-array #f
                                (vector-length new-rows)
                                columns)))
    (copy-rows! new-array 0 old-array 0 row)
    (copy-rows! new-array (1+ row)
                old-array (+ 2 row) (vector-length (grid-rows grid)))
    (do ((i 0 (1+ i))
         (rowvec-up (array-cell-ref old-array row))
         (rowvec-down (array-cell-ref old-array (1+ row))))
        ((>= i columns))
      (array-fill! (array-slice new-array row i)
                   (proc (array-ref rowvec-up i)
                         (array-ref rowvec-down i))))
    (make-grid new-array (grid-columns grid) new-rows)))

(define (merge-columns grid column proc)
  "Merge COLUMN with (1+ COLUMN).
For each two merging cells call (PROC left-cell right-cell) which
should return a value for the new cell."
  (when (< (vector-length (grid-columns grid)) 2)
    (error "At least two grid columns are required."))
  (let* ((old (make-grid (transpose-array (grid-array grid) 1 0)
                         (grid-rows grid)
                         (grid-columns grid)))
         (new (merge-rows old column proc)))
    (make-grid (transpose-array (grid-array new) 1 0)
               (grid-rows new)
               (grid-columns new))))

(define (resize-grid grid size-x size-y cell-min-x cell-min-y)
  "Change the size of GRID to SIZE-X and SIZE-Y.
Proportionally resize all GRID cells but do not reduce their size to
less than CELL-MIN-X and CELL-MIN-Y.  Return a new grid with the same
array and new columns and rows."
  (define (resize vector size min)
    (define k (/ (vector-sum vector) size))
    (vector-map (lambda (i v)
                  (max (/ v k) min))
                vector))
  (make-grid (grid-array grid)
             (resize (grid-columns grid) size-x cell-min-x)
             (resize (grid-rows grid) size-y cell-min-y)))

(define* (grid-size grid #:optional axis)
  (if axis
      (vector-sum ((case axis
                     ((x) grid-columns)
                     ((y) grid-rows)
                     (else (error "Wrong axis name: ~a" axis))) grid))
      (values (grid-size grid 'x) (grid-size grid 'y))))

(define-record-type <window>
  (make-window buffer)
  window?
  (buffer window-buffer))

(define-record-type <tiles>
  (%make-tiles grid active-window pressed-keys mouse-buttons mouse-x mouse-y)
  tiles?
  (grid tiles-grid set-tiles-grid!)
  (active-window tiles-active-window set-tiles-active-window!)
  (pressed-keys tiles-pressed-keys)
  (mouse-buttons tiles-mouse-buttons)
  (mouse-x tiles-mouse-x)
  (mouse-y tiles-mouse-y)
  (render-proc tiles-render-proc set-tiles-render-proc!)
  (viewport-proc tiles-viewport-proc set-tiles-viewport-proc!))

(define window-margin 10)
(define window-min-size (+ 40 (* 2 window-margin)))
(define window-background-color (list 0f0 0f0 0f0 1f0))
(define active-window-bar-color (list .8f0 .8f0 .8f0 1f0))

(define* (window-size index slice vector #:optional (test eq?))
  "Calculate the size of a window at INDEX.
A window can spans more than one cell in SLICE.  Test the window cell
spanning while (TEST cell-INDEX cell-INDEX-n) is true.  Return two
values: the window size and the last+1 index."
  (let ((len (vector-length vector))
        (val (array-ref slice index)))
    (let loop ((i (1+ index))
               (size (vector-ref vector index)))
      (if (and (< i len)
               (test val (array-ref slice i)))
          (loop (1+ i) (+ size (vector-ref vector i)))
          (values size i)))))

(define (fill-new-window grid new old row column)
  (define lenr (grid-rows-length grid))
  (define lenc (grid-columns-length grid))
  (let lpr ((r row))
    (when (and (< r lenr)
               (eq? (grid-array-ref grid r column) old))
      (let lpc ((c column))
        (when (and (< c lenc)
                   (eq? (grid-array-ref grid r c) old))
          (list r c)
          (grid-array-set! grid new r c)
          (lpc (1+ c))))
      (lpr (1+ r)))))

(define (split-window-horizontally grid row column location)
  (let* ((old-window (grid-array-ref grid row column))
         (new-window (make-window (window-buffer old-window))))
    (receive (new-grid new-row)
        (split-row grid location
                   (lambda (window)
                     (if (eq? window old-window)
                         (values window new-window)
                         (values window window))))
      (fill-new-window new-grid new-window old-window new-row column)
      (values new-grid new-window))))

(define (split-window-vertically grid row column location)
  (let* ((old-window (grid-array-ref grid row column))
         (new-window (make-window (window-buffer old-window))))
    (receive (new-grid new-column)
        (split-column grid location
                      (lambda (window)
                        (if (eq? window old-window)
                            (values window new-window)
                            (values window window))))
      (fill-new-window new-grid new-window old-window row new-column)
      (values new-grid new-window))))

(define (make-windows-rects tiles)
  (define rects '())
  (match-let ((($ <grid> array columns rows) (tiles-grid tiles)))
    (letrec
        ((mkrects
          (lambda (col row x1 y1)
            (receive (sizex col2)
                (window-size col (array-cell-ref array row) columns)
              (receive (sizey row2)
                  (window-size row (make-shared-array
                                    array (lambda (i) (list i col))
                                    (car (array-dimensions array)))
                               rows)
                (let ((x2 (+ x1 sizex))
                      (y2 (+ y1 sizey)))
                  (set! rects
                        (cons (make-rect (+ x1 window-margin)
                                         (+ y1 window-margin)
                                         (- x2 window-margin)
                                         (- y2 window-margin)
                                         #:color window-background-color)
                              rects))
                  (when (eq? (tiles-active-window tiles)
                             (array-ref array col row))
                    (set! rects
                        (cons (make-rect (+ x1 window-margin)
                                         (- y2 window-margin -1)
                                         (- x2 window-margin)
                                         (- y2 window-margin -4)
                                         #:color active-window-bar-color)
                              rects)))
                  (let ((right (if (< col2 (vector-length columns))
                                   (begin (mkrects col2 row (+ x1 sizex) y1) #t)
                                   #f))
                        (down (if (< row2 (vector-length rows))
                                  (begin (mkrects col row2 x1 (+ y1 sizey)) #t)
                                  #f)))
                    (and right down
                         (mkrects col2 row2
                                  (+ x1 sizex)
                                  (+ y1 sizey))))))))))
      (mkrects 0 0 0 0)
      rects)))

(define (render tiles renderer frame-size-x frame-size-y swap-buffers)
  ((tiles-viewport-proc tiles))
  ;; TODO Make the background color as a preference
  (set-gl-clear-color .2 .2 .2 1.)
  (gl-clear (clear-buffer-mask color-buffer))
  (draw-rects renderer frame-size-x frame-size-y (make-windows-rects tiles))
  (swap-buffers))

(define (set-render-proc! tiles)
  (set-tiles-render-proc! tiles
                          (receive (sx sy) (grid-size (tiles-grid tiles))
                            (lambda (renderer swap-buffers)
                              (render tiles
                                      renderer
                                      sx sy
                                      swap-buffers)))))

(define-inlinable (unset-render-proc! tiles)
  (set-tiles-render-proc! tiles (const #f)))

(define (set-viewport-proc! tiles width height)
  (set-tiles-viewport-proc! tiles
                            (lambda ()
                              (gl-viewport 0 0 width height))))

(define-inlinable (unset-viewport-proc! tiles)
  (set-tiles-viewport-proc! tiles (const #f)))

(define-public (make-tiles frame-size-x frame-size-y)
  (let* ((window (make-window 'make-message-buffer))
         (tiles (%make-tiles (make-new-grid window frame-size-x frame-size-y)
                             window
                             '() '() 0 0)))
    (set-render-proc! tiles)
    (receive (w h) (grid-size (tiles-grid tiles))
      (set-viewport-proc! tiles w h))
    tiles))

(define-public (tiles-resize tiles size-x size-y)
  (set-tiles-grid! tiles
                   (resize-grid (tiles-grid tiles)
                                size-x size-y
                                window-min-size window-min-size))
  (set-render-proc! tiles)
  (receive (w h) (grid-size (tiles-grid tiles))
    (set-viewport-proc! tiles w h)))

(define-public (tiles-mouse-move tiles x y)
  (set-render-proc! tiles))

(define-public (tiles-mouse-button tiles button action mods)
  (set-render-proc! tiles))

(define-public (render-tiles tiles renderer swap-buffers)
  ((tiles-render-proc tiles) renderer swap-buffers)
  (unset-render-proc! tiles)
  (unset-viewport-proc! tiles))
