;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Mes.
;;;
;;; Mes is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Mes is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Mes.  If not, see <http://www.gnu.org/licenses/>.

(define-module (mes gui)
  #:use-module (ice-9 match)
  #:use-module (8sync)
  #:use-module (gl)
  #:use-module (mes gl)
  #:use-module (mes render)
  #:use-module (mes tiles)
  #:use-module ((glfw) #:renamer (lambda (n)
                                   (case n
                                     ((init) 'glfw-init)
                                     ((terminate) 'glfw-terminate)
                                     (else n))))
  #:export (<gui>))

(current-gl-resolver (lambda (name)
                       (dynamic-link "libGL")
                       (current-gl-resolver get-proc-address)
                       (get-proc-address name)))

(define-actor <gui> (<actor>)
  ((*init* gui-init)
   (*cleanup* gui-cleanup)
   (loop gui-loop))
  (glfw-window #:init-value #f
               #:accessor gui-glfw-window)
  (renderer #:init-value #f
            #:accessor gui-renderer)
  (tiles #:init-value #f
         #:accessor gui-tiles))

(define (setup-callbacks gui)
  (let ((window (gui-glfw-window gui))
        (tiles (gui-tiles gui)))
    (set-window-size-callback! window
                               (lambda (w x y)
                                 (tiles-resize tiles x y)))
    (set-cursor-pos-callback! window
                              (lambda (w x y)
                                (tiles-mouse-move tiles x y)))
    (set-mouse-button-callback! (gui-glfw-window gui)
                                (lambda (w b a m)
                                  (tiles-mouse-button tiles b a m)))))

(define (gui-init gui message)
  (glfw-init)
  (set-error-callback! (lambda (code string)
                         (error "GLFW error: ~A ~A" code string)))
  (window-hint 'context-version-major 3)
  (window-hint 'context-version-minor 3)
  (window-hint 'opengl-profile 'opengl-core-profile)
  ;; TODO Make width and height as preferences
  (let ((window (create-window 1024 768 "Mes")))
    (set! (gui-glfw-window gui) window)
    (make-context-current! window)
    (set! (gui-renderer gui) (init-render))
    (set! (gui-tiles gui) (apply make-tiles (get-window-size window #t)))
    (setup-callbacks gui)
    (<- (actor-id gui) 'loop)))

(define (gui-cleanup gui message)
  (when (gui-renderer gui)
    (close-render (gui-renderer gui)))
  (destroy-window! (gui-glfw-window gui))
  (glfw-terminate))

(define (gui-loop gui message)
  (poll-events)
  (if (window-should-close? (gui-glfw-window gui))
      (self-destruct gui)
      (begin
        (render-tiles (gui-tiles gui)
                      (gui-renderer gui)
                      (lambda () (swap-buffers (gui-glfw-window gui))))
        (8sleep 1/30)              ;TODO (8sleep (- 1/30 render-time))
        (<- (actor-id gui) 'loop))))
